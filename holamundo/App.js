import React from 'react';
import { Button, Image, Linking, StyleSheet, Text, View } from 'react-native';
import {
    MaterialIcons,
    FontAwesome,
    Zocial,
    EvilIcons,
} from '@expo/vector-icons';

// Componente Nativo (Arrow Function)
const App = () => {
    return (
        <View style={estilos.contenedor}>
            <View
                style={[
                    estilos.contenedor,
                    {
                        backgroundColor: '#00587a',
                        alignItems: 'center',
                        justifyContent: 'center',
                    },
                ]}
            >
                {/* 
      Ejemplo de imagen con recurso local
      source={require('./assets/foto_perfil.jpeg')} 
      */}

                <Image
                    source={{
                        uri:
                            'https://toppng.com/uploads/preview/ara-perfil-do-discord-11563369009tj496eh26g.png',
                    }}
                    style={{
                        marginTop: 50,
                        width: 150,
                        height: 150,
                        borderRadius: 150,
                        resizeMode: 'center',
                    }}
                />

                <Text
                    style={{
                        fontSize: 22,
                        marginBottom: 20,
                        textAlign: 'center',
                        color: '#fff',
                        textDecorationLine: 'underline',
                    }}
                >
                    TSU en Desarrollo de Software Multiplataforma
                </Text>

                <View style={[estilos.contenedorContacto]}>
                    <Text
                        style={[
                            estilos.contenedor,
                            estilos.textoContacto,
                            { textAlign: 'left' },
                        ]}
                    >
                        <MaterialIcons name='email' size={18} />
                        {'  '}
                        correo@uteq.edu.mx
                    </Text>
                    <Text
                        style={[
                            estilos.contenedor,
                            estilos.textoContacto,
                            { textAlign: 'right' },
                        ]}
                    >
                        <MaterialIcons name='phone-android' size={18} />
                        {'  '}
                        +52 (442) 396 8482
                    </Text>
                </View>

                <Text
                    style={[
                        estilos.contenedor,
                        estilos.textoContacto,
                        {
                            marginVertical: 20,
                            textAlign: 'center',
                            fontStyle: 'italic',
                        },
                    ]}
                >
                    "La juventud no es un periodo de la vida sino un estado del
                    espíritu humano"
                </Text>
            </View>

            {/* SECCION INFERIOR */}

            <View style={[estilos.contenedor, estilos.contenedorInf]}>
                <View style={[estilos.contenedorRedesSociales]}>
                    <Text
                        style={[
                            estilos.contenedorRedesSociales,
                            { color: 'blue' },
                        ]}
                        onPress={() => Linking.openURL('http://facebook.com')}
                    >
                        <MaterialIcons name='facebook' size={30} />
                        {'  '}
                        Facebook
                    </Text>

                    <Text
                        style={[
                            estilos.contenedorRedesSociales,
                            { color: 'red' },
                        ]}
                        onPress={() => Linking.openURL('http://youtube.com')}
                    >
                        <FontAwesome name='youtube' size={30} />
                        {'  '}
                        YouTube
                    </Text>

                    <Text
                        style={[
                            estilos.contenedorRedesSociales,
                            { color: 'aqua' },
                        ]}
                        onPress={() => Linking.openURL('http://twitter.com')}
                    >
                        <FontAwesome name='twitter' size={30} />
                        {'  '}
                        Twitter
                    </Text>
                </View>

                <Image
                    source={require('./assets/datosContactoQR.png')}
                    style={{
                        width: 200,
                        height: 200,
                        alignSelf: 'center',
                        marginTop: 20,
                    }}
                />

                <Text
                    style={[
                        estilos.contenedor,
                        {
                            marginVertical: 20,
                            textAlign: 'center',
                            fontStyle: 'normal',
                            color: 'black',
                        },
                    ]}
                >
                    Escanea toda mi información o accede
                </Text>

                <Text
                    style={[
                        {
                            color: '#07689f',
                            alignSelf: 'center',
                            marginBottom: 20,
                        },
                    ]}
                    onPress={() => Linking.openURL('https://www.linkedin.com/')}
                >
                    <FontAwesome name='linkedin' size={30} />
                    {'  '}
                    Linkedin
                </Text>
            </View>
        </View>
    );
};

const estilos = StyleSheet.create({
    contenedor: {
        // Uso de flex
        flex: 1,
    },

    contenedorInf: {
        backgroundColor: '#e7e7de',
    },

    contenedorContacto: {
        flexDirection: 'row',
        width: '90%',
    },

    textoContacto: {
        color: '#fff',
        fontSize: 15,
    },

    contenedorRedesSociales: {
        marginVertical: 10,
        flexDirection: 'row',
        marginHorizontal: 15,
    },
});

export default App;
