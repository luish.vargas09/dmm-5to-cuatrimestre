import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Platform, ScrollView, StyleSheet, Text, View } from 'react-native';
// Platform es una libreria de React que te permite identificar en que plataforma está
// corriendo el código fuente y definir algún cambio único para cada plataforma IOS/Android
import Hola from './src/components/Hola';
import MiComponenteAF, { OtraAF } from './src/components/MiComponenteAF';
import MiComponenteClass, {
  OtraClase,
} from './src/components/MiComponenteClass';

// Componente en React Native:

// - Simple Components:
// Es una sección de código (Fragment, item)

// - Screen Components:
// UI completa, presentación de la aplicación

// - Functional Components:
// Sección de código que se conectan a una librería
// o generan alguna funcionalidad (p.e. conexión a una BD, insert valores, etc)

// Los compoenentes pueden crearse de 3 formas distintas:
// 1.- Constantes (Arrow Function)
// 2.- Funciones
// 3.- Clases

// Importamos nuestro componente que ya exportamos desde otro archivo:
// import MODULO* from './RUTA_RELATIVA'

// *Los modulos pueden tener export default o no
// - SI se trata del export default, solo nombramos el componente
// - si NO es export default, se verá entre llaves { } el nombre del componente

//Y si queremos mandar llamar al otro componente tendriamos que indicarlo entre llaves
import MiComponenteFn, {
  OtroComponente,
} from './src/components/MiComponenteFn';

export default function App() {
  return (
    <ScrollView>
      <View style={styles.container}>
        {/* margin-top -> marginTop
            margin-left -> marginLeft
            text-size -> textSize 
        */}
        <Text
          style={{
            marginTop: 40,
            fontSize: 30,
            fontWeight: 'bold',
            color: Platform.OS === 'ios' ? '#000FFF' : '#FFF', // Azul = IOS y blanco = Android
            backgroundColor: Platform.OS === 'android' ? '#ff0000' : '#000', // Rojo = Android y Negro = Apple
            padding: 20,
            borderRadius: 10,
            width: '100%',
            textAlign: 'center',
            marginBottom: 30,
          }}
        >
          Hola Mundo Cruel...
        </Text>

        <Text style={styles.subtitulos}>
          Prueba de estilo llamado Subtítulo
        </Text>

        {/* Agregamos los componentes que queramos */}
        <Espacios />

        <MiComponenteFn />
        <OtroComponente />

        <Espacios />

        <MiComponenteClass />
        <OtraClase />

        <Espacios />

        <MiComponenteAF />
        <OtraAF />

        <Espacios />

        <Hola />

        <StatusBar style='auto' />
      </View>
    </ScrollView>
  );
}

// Componente que genera espacios por medio de vistas
export const Espacios = () => {
  return (
    <>
      <View>
        {/* Espacio en blanco */}
        <Text>{'    '}</Text>
      </View>
      <View>
        {/* Espacio en blanco */}
        <Text>{'    '}</Text>
      </View>
    </>
  );
};

// Estilos Globales
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  subtitulos: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#fff',
    backgroundColor: '#000',
    padding: 10,
    // borderRadius: 10,
    width: '100%',
    textAlign: 'center',
  },
});
