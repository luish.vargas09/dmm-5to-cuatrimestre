import React from 'react';
import { Text, View } from 'react-native';

// Agregamos librerías de íconos
import { EvilIcons, Entypo, Feather } from '@expo/vector-icons';

// El diseño de UI en React usa dos principios del desarrollo web
// entre ellos el manejo de JSX, los estilos también están inspridados
// en el entorno web.

//Flex
const App = () => {
  return (
    <View style={{ flex: 1 }}>
      <View
        style={{
          backgroundColor: '#F6D8AE',
          flex: 8,
          alignItems: 'center',
          justifyContent: 'space-evenly',
        }}
      >
        <Text>
          <EvilIcons name='user' size={30} color='#083D77' />
          {'  '}
          Luis Humberto
        </Text>

        <Text>
          <Entypo name='mobile' size={30} color='#083D77' />
          {'  '}
          4423968482
        </Text>

        <Text>
          <Feather name='mail' size={20} color='#083D77' />
          {'  '}
          luish.vargas09@gmail.com
        </Text>
      </View>

      <View
        style={{ backgroundColor: '#083D77', flex: 2, flexDirection: 'row' }}
      >
        <View style={{ backgroundColor: '#DA4167', flex: 1 }} />
        <View style={{ backgroundColor: '#2E4057', flex: 1 }} />

        <View style={{ backgroundColor: '#FFFFFF', flex: 1 }}>
          <View style={{ backgroundColor: '#FFFFFF', flex: 1 }} />
          <View style={{ backgroundColor: '#000000', flex: 1 }} />
        </View>

        <View style={{ backgroundColor: '#083D77', flex: 1 }} />
        <View style={{ backgroundColor: '#6E2594', flex: 1 }} />
      </View>
    </View>
  );
};

export default App;
