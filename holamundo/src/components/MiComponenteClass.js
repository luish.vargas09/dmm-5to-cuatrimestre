import React, { Component } from 'react';
import { Button, View, Text } from 'react-native';
import MisEstilos from './../styles/estilos';

//Heredar de la clase Component, solo cuando usamos clases
export default class MiComponenteClass extends Component {
  //Las clases de componente de React retornan el contenido por medio del metodo render()
  render() {
    return (
      <>
        <Text style={MisEstilos.subtitulos}>Mi Componente como Clase</Text>
        <Button title='Boton 2 (Clase)' />
        <Text> </Text>
        <Button title='Boton 3 (Clase)' />
      </>
    );
  }
}

export class OtraClase extends Component {
  render() {
    return (
      <View>
        <Text style={MisEstilos.subtitulos}>
          Otra componente desde una Clase
        </Text>
      </View>
    );
  }
}
