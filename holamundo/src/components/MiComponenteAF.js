import React from 'react';
import { Button, Text } from 'react-native';
import MisEstilos from './../styles/estilos';

// Con AF no se puede poner export default cuando trabajamos con constantes
const MiComponenteAF = () => {
  // Creamos una funcion flecha con el comportamiento del click
  //como no es un componente empieza con minúscula
  const accionClick = () => {
    console.log('Me dieron click desde una AF');
  };

  return (
    <>
      <Button title='Boton 4' />
      <Button title='Boton 5' onPress={accionClick} />
    </>
  );
};

export const OtraAF = () => {
  return (
    <>
      <Text style={MisEstilos.subtitulos}>
        Otra componente desde una Arrow Function{' '}
      </Text>
    </>
  );
};

export default MiComponenteAF;
