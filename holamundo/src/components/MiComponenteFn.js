/*
Lo mínimo para crear un componente:
    1.- Importar React de las librerias del core
    2.- Importar los elementos visuales requeridos (min 1)
    3.- Retornar mínimo un lemento visual
    4.- Exportar el módulo principal para echarlo andar
*/

// 1.- Librería React del core
import React from 'react';

// 2.- Elementos visuales requeridos en este componente
import { Button, Text, View } from 'react-native';
import MisEstilos from './../styles/estilos';

// 4.- Exportamos el componente
export default function MiComponenteFn() {
  // 3.- Elemento visual que retornaremos
  return (
    // Esto <>...</> le indica a React que use fragmentos
    // para mandar dos elementos a la vez
    <>
      <View>
        <Text style={MisEstilos.subtitulos}>Mi Componente como Función</Text>
      </View>
      <View>
        <Button title='Boton 1' />
      </View>
    </>
  );
}

// Se puede hacer el paso 4 de dos maneras:
// - en la función al inicio
// - o así... export default MiComponenteFn;

// Se pueden crear tantos componentes como queramos
// pero solo puede haber un default por archivo

export function OtroComponente() {
  return (
    <View>
      <Text style={MisEstilos.subtitulos}>
        Otro Componente desde una Función
      </Text>
    </View>
  );
}
