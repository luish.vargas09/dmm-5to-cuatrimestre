import React from 'react';
import { Button, Text, TextInput } from 'react-native';
import MisEstilos from './../styles/estilos';
import { Espacios } from '../../App.back.intro_components';

const Hola = () => {
  return (
    <>
      <Text style={MisEstilos.subtitulos}>Nombre: </Text>
      <Text> </Text>
      <TextInput
        style={MisEstilos.input}
        placeholder='Nombre'
        keyboardType='default'
      />

      {/* React utiliza funciones anonimas en su forma de AF para trabajar con los eventos */}
      {/* Todos los onPress esperan un AF que haga algo */}
      <Button
        title='Hola'
        onPress={() => {
          console.log('Me dieron click');
        }}
      />

      {/* Agregamos un espacio adicioanl al final, SOLO para Android */}
      {Platform.OS === 'android' ? <Espacios /> : null}
    </>
  );
};

export default Hola;
