import React from 'react';
import { StyleSheet } from 'react-native';

// Objeto de estilos sin nombre a exportar
export default {
  subtitulos: {
    fontSize: 15,
    fontWeight: 'bold',
    color: '#fff',
    backgroundColor: '#000',
    padding: 10,
    borderRadius: 10,
    width: '100%',
    textAlign: 'center',
  },
  input: {
    borderColor: '#000',
    boderWidth: 2,
    width: '95%',
    paddingVertical: 5,
    paddingHorizontal: 10,
    marginVertical: 5,
    marginHorizontal: 10,
    fontSize: 16,
    borderRadius: 10,
  },
};
