import React, { useEffect } from 'react';
// import Loginn from './src/screens/Loginn';
import Login from './src/screens/Login';
// import Inicioo from './src/screens/Inicioo';
import Inicio from './src/screens/Inicio';
// import Registroo from './src/screens/Registroo';
import Registro from './src/screens/Registro';
import Home from './src/screens/private/Home';
import ControlRemoto from './src/screens/ControlRemoto';
// LogBox hace que ya no se vean los warnings
import { LogBox } from 'react-native';

// En App.js ya no lleva componentes gráficos, más bien es un navigation router, es decir,
// es el que sabe cual es la 1er pantalla, la 2da o sabe donde están todas las Screens

// Para crear un esquema de navegación en React, necesitamos:
// 1.- NavigationContainer (uno por App)
// 2.- Seleccionar un tipo de navegación como contenedor de los componentes visuales (Screens)
// 3.- Agregar cada Screen al tipo de navegación con un sobrenombre
// 4.- Disfrutar de la navegación

// Nota: Las versiones de las librerías deberían ser compatibles entre ellas

// Paso 1:
import { NavigationContainer } from '@react-navigation/native';

// Paso 2:
import { createStackNavigator } from '@react-navigation/stack';

// Paso 2.1:
const Stack = createStackNavigator();

// Función principal
export default function App() {
    // Hook al inicio que indique para toda la app que no se muestre
    // mas el mensaje de warning para 'NativeDriver'
    useEffect(() => {
        // indicamos los tipos de warnings que queremos dejar de ver

        // TODOS
        // LogBox.ignoreAllLogs();

        // Solo algunos warnings
        LogBox.ignoreLogs([
            'Animated: `useNativeDriver`',
            'Setting a timer for a long period of time',
        ]);
    }, []);

    // Creamos en App.js un enrutador de las pantallas navegables de la app por medio de un Screen
    return (
        // Paso 1: Un NavigationContainer por app
        <NavigationContainer>
            {/* Paso 2: Le decimos donde va iniciar */}
            <Stack.Navigator initialRouteName='Login' headerMode='float'>
                {/* Paso 3: Indicamos todas las Screen relacionadas */}
                {/* Componente padre = Stack | Componente hijo = Screen */}
                {/* <Stack.Screen name='Loginn' component={Loginn} /> */}
                <Stack.Screen name='Login' component={Login} />
                {/* <Stack.Screen name='Inicioo' component={Inicioo} /> */}
                <Stack.Screen name='Inicio' component={Inicio} />
                {/* <Stack.Screen name='Registroo' component={Registroo} /> */}
                <Stack.Screen name='Registro' component={Registro} />
                <Stack.Screen name='Home' component={Home} />
                <Stack.Screen name='ControlRemoto' component={ControlRemoto} />
                {/* name = sobrenombre | component = screens importadas */}
            </Stack.Navigator>
        </NavigationContainer>
    );
}
