import { DrawerContentScrollView, DrawerItem } from '@react-navigation/drawer';
import React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import { AntDesign, MaterialIcons } from '@expo/vector-icons';

const Sidebar = (props) => {
    return (
        <View
            style={{
                flex: 1,
            }}
        >
            <ImageBackground
                source={require('./../../assets/images/CineDownload.jpg')}
                style={{ width: '100%', paddingBottom: 30, opacity: 0.8 }}
            >
                <Text
                    style={{
                        marginTop: 15,
                        width: '100%',
                        textAlign: 'center',
                        fontSize: 20,
                        fontWeight: 'bold',
                        textDecorationLine: 'underline',
                        color: '#e40017',
                    }}
                >
                    BIENVENIDO
                </Text>

                <View style={{ flexDirection: 'row' }}>
                    <View style={{ flex: 1, alignItems: 'center' }}>
                        <ImageBackground
                            source={require('./../../assets/images/foto_perfil4.png')}
                            style={{
                                width: 80,
                                height: 80,
                                overflow: 'hidden',
                                marginVertical: 20,
                                borderRadius: 80,
                                backgroundColor: '#666',
                                borderColor: '#000',
                                borderWidth: 2,
                                resizeMode: 'contain',
                            }}
                        />
                    </View>
                    <View style={{ flex: 2 }}>
                        <View
                            style={{ flex: 1, marginLeft: 10, marginTop: 35 }}
                        >
                            <Text
                                style={{
                                    fontSize: 18,
                                    marginBottom: 5,
                                    color: '#fff',
                                    fontWeight: 'bold',
                                }}
                            >
                                Humberto Vargas
                            </Text>
                            <Text
                                style={{
                                    fontSize: 16,
                                    marginBottom: 5,
                                    fontStyle: 'italic',
                                    fontWeight: 'bold',
                                    color: '#d2e603',
                                }}
                            >
                                0 Rentas
                            </Text>
                        </View>
                    </View>
                </View>
            </ImageBackground>

            <DrawerContentScrollView {...props}>
                <DrawerItem
                    icon={() => (
                        <AntDesign name='home' size={20} color='#000' />
                    )}
                    label='Inicio'
                    onPress={() => {
                        props.navigation.navigate('InicioUser');
                    }}
                />
                <DrawerItem
                    icon={() => (
                        <AntDesign name='user' size={20} color='#000' />
                    )}
                    label='Perfil'
                    onPress={() => {
                        props.navigation.navigate('Perfil');
                    }}
                />

                <DrawerItem
                    icon={() => (
                        <MaterialIcons
                            name='local-movies'
                            size={20}
                            color='#000'
                        />
                    )}
                    label='Catálogo'
                    onPress={() => {
                        props.navigation.navigate('Catalogo');
                    }}
                />

                <DrawerItem
                    icon={() => (
                        <MaterialIcons
                            name='movie-filter'
                            size={20}
                            color='#000'
                        />
                    )}
                    label='Practica3'
                    onPress={() => {
                        props.navigation.navigate('Practica3');
                    }}
                />

                <DrawerItem
                    icon={() => (
                        <MaterialIcons
                            name='location-on'
                            size={20}
                            color='#000'
                        />
                    )}
                    label='Geolocalización'
                    onPress={() => props.navigation.navigate('Geoloc')}
                />

                <DrawerItem
                    icon={() => (
                        <MaterialIcons
                            name='notifications-active'
                            size={20}
                            color='#000'
                        />
                    )}
                    label='Notificaciones'
                    onPress={() => props.navigation.navigate('Notificaciones')}
                />
            </DrawerContentScrollView>
        </View>
    );
};

export default Sidebar;
