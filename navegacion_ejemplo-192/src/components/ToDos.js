import React from 'react';
import { ImageBackground, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {
    FontAwesome5,
    AntDesign,
    MaterialCommunityIcons,
} from '@expo/vector-icons';

const ToDos = ({ datosToDos }) => {
    // console.log(datosToDos);

    // const { datosUsuario } = props;

    // Object Destructuring: (INVESTIGAR)
    // Es la posibilidad de convertir en variables//constantes las claves de un objeto
    const { title, completed } = datosToDos;

    return (
        <TouchableOpacity>
            <View
                style={{
                    backgroundColor: '#fff',
                    padding: 20,
                    borderRadius: 20,
                    margin: 10,
                    flex: 1,
                    shadowColor: '#666',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.5,
                    shadowRadius: 2,
                    elevation: 4,
                }}
            >
                <View style={{ flexDirection: 'row' }}>
                    <View
                        style={{
                            flex: 2,
                            alignItems: 'flex-start',
                            justifyContent: 'center',
                        }}
                    >
                        <MaterialCommunityIcons
                            name='star-four-points'
                            size={18}
                            color='#008891'
                        />
                    </View>

                    <View style={{ flex: 6, justifyContent: 'center' }}>
                        <Text
                            style={{
                                fontSize: 15,
                                fontWeight: 'bold',
                            }}
                        >
                            {' '}
                            {title}
                            {/* {completed}{' '} */}
                        </Text>
                    </View>

                    <View
                        style={{
                            flex: 3,
                            alignItems: 'flex-end',
                            justifyContent: 'space-between',
                        }}
                    >
                        <TouchableOpacity
                            style={{
                                // backgroundColor: '#ccc',
                                padding: 10,
                                margin: 5,
                                borderRadius: 20,
                            }}
                        >
                            {completed ? (
                                <AntDesign
                                    name='checkcircleo'
                                    size={20}
                                    color='#00b906'
                                />
                            ) : (
                                <FontAwesome5
                                    name='times-circle'
                                    size={20}
                                    color='#ff0000'
                                />
                            )}
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
};

export default ToDos;
