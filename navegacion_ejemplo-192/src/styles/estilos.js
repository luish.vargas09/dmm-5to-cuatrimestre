import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    contenedorGral: {
        flex: 1,
    },
    contenedor: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    titulo: {
        textAlign: 'center',
        fontSize: 25,
        fontWeight: 'bold',
        marginVertical: 10,
        marginBottom: 30,
    },
    input: {
        paddingHorizontal: 10,
        paddingVertical: 5,
        width: '95%',
        borderWidth: 2,
        borderColor: '#585858',
        fontSize: 16,
        borderRadius: 10,
        marginVertical: 5,
    },
    imgLogin: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
        borderRadius: 200,
    },
    imgRegistro: {
        height: 200,
        width: 200,
        resizeMode: 'contain',
    },
    switch: {
        marginVertical: 20,
    },
    button: {
        marginVertical: 100,
        paddingVertical: 100,
        backgroundColor: '#0909',
    },

    boton1: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 185,
        height: 50,
        backgroundColor: '#008891',
        borderRadius: 10,
        marginVertical: 7,
    },

    boton2: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 185,
        height: 50,
        backgroundColor: '#d7385e',
        borderRadius: 10,
        marginVertical: 5,
    },

    botonAbrir: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 185,
        height: 50,
        backgroundColor: 'green',
        borderRadius: 10,
        marginVertical: 5,
    },

    botonCerrar: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 185,
        height: 50,
        backgroundColor: 'red',
        borderRadius: 10,
        marginVertical: 5,
    },

    textoBoton: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 18,
    },
    textoLogo: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 40,
        marginBottom: 10,
    },

    textPantalla: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 35,
    },

    textCard: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 20,
        backgroundColor: '#008891',
        opacity: 0.7,
        textAlign: 'center',
        alignContent: 'center',
        alignItems: 'center',
        alignSelf: 'center',
        width: 300,
        height: 40,
    },

    textInfo: {
        color: '#666666',
        fontWeight: 'bold',
        fontSize: 16,
        alignSelf: 'flex-start',
        marginStart: 16,
        marginTop: 10,
    },

    contenedorGral: {
        flex: 1,
    },

    contenedorSuperior: {
        flex: 1,
        backgroundColor: '#00587a',
        alignItems: 'center',
        justifyContent: 'center',
    },

    contenedorFondo: {
        flex: 1,
        backgroundColor: '#000',
        alignItems: 'center',
        justifyContent: 'center',
    },

    card: {
        flex: 1,
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        width: 250,
        height: 250,
        // backgroundColor: '#000',
        borderRadius: 30,
        marginVertical: 10,
    },

    imageFondo: {
        flex: 1,
        resizeMode: 'cover',
        justifyContent: 'center',
    },
});
