// Función que muestre el error en español/méxico

export default function get_error(tipo) {
    switch (tipo) {
        case 'auth/email-already-in-use':
            return 'El correo ya se encuentra asociado a otra cuenta existente';
        case 'auth/invalid-email':
            return 'Correo electrónico inváldo';
        case 'auth/user-not-found':
            return 'Usuario no encontrado';
        case 'auth/wrong-password':
            return 'Contraseña incorrecta';
    }
}
