// Importo todos los servicios de firebase:
// 1.- firestore
// 2.- authentification
// 3.- storage
// 4.- hosting
import firebase from 'firebase';
import 'firebase/firestore';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: 'AIzaSyDtHLa4suh_rm11gLB0Jk00n1zsIiluvQ8',
    authDomain: 'dmm-192-9b419.firebaseapp.com',
    projectId: 'dmm-192-9b419',
    storageBucket: 'dmm-192-9b419.appspot.com',
    messagingSenderId: '291845008193',
    appId: '1:291845008193:web:1cf69de9e830a78f8d0fc0',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

// Retornamos los servicios de firebase
const db = firebase.firestore();
const auth = firebase.auth();
const storage = firebase.storage();

// Generamos una librería reutilizable
export default { db, auth, storage };
