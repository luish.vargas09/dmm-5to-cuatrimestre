import React, { useState } from 'react';
import { Button, Text, View } from 'react-native';

const Loginn = (props) => {
    // Las variables comunes (let, const, obj, etc) no son modificables dentro del render
    // render es el objeto return en una función y en una clase pues si es render

    // Si necesitas modificar algún valor de la UI es necesario que utilices ESTADOS(state)

    // Los estados son variables enganchadas al VDOM que permiten su modificación por medio
    // de una función (ya que los estados son de solo lectura).

    // Para usar un estado utilizamos la librería "useState" de React
    // sintaxis: const [valor, setValor] = useState(_VALOR_INICIAL_);
    // snippet React: usf+ctrl+space
    const [suma, setSuma] = useState(-10);
    return (
        <View
            style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            <Text>En login.js</Text>

            <Button
                title='Login'
                onPress={() => {
                    // Para navegar entre ventanas usamos:
                    // - propiedad: "navigation"
                    // - función: "navigate"
                    // - parámetro: "name del componente/screen a ir"
                    props.navigation.navigate('Inicio');
                }}
            />

            <Text
                style={{
                    fontSize: 120,
                    textAlign: 'center',
                    fontWeight: '100',
                    marginVertical: 40,
                }}
            >
                {suma}
            </Text>

            <Button
                title='Sumar: +1'
                onPress={() => {
                    // Dado que suma es una constante, no podemos modificar su
                    // valor directamente, para ello utilizamos su función setSuma()
                    setSuma(suma + 1);
                    console.log(suma);
                    // console.log(props);
                }}
            />
        </View>
    );
};

export default Loginn;
