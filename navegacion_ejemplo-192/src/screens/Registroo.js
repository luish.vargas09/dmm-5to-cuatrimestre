import React, { Component } from 'react';
import { Button, Text, View } from 'react-native';

export default class Registroo extends Component {
    // Para generar estados en una clase, debemos inicializar el objeto global "state"
    // dentro del constructor, al igual que las propiedades
    constructor(props) {
        // Para llamar a las props utilizamos this.props
        super(props);
        // Las clases tienen un atributo tipo objeto reservado que se llama "state",
        // aquí debemos guardar todas las variables de estado que sean necesarias
        this.state = {
            suma: -25,
            title: 'Home',
        };
    }

    render() {
        return (
            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <Button
                    title={this.state.title}
                    onPress={() => {
                        this.props.navigation.navigate('Inicio');
                    }}
                ></Button>

                <Text
                    style={{
                        fontSize: 50,
                        textAlign: 'center',
                        fontWeight: '100',
                        marginVertical: 40,
                    }}
                >
                    {/* Para invocar una varibale de estado: this.state._NOMBRE_VAR_ */}
                    {this.state.suma}
                </Text>
                {/* Para actualizar el valor de un estado invocamos a la función this.setState(_OBJ(s)_ESTADO_) */}
                <Button
                    title='Sumar: +1'
                    onPress={() => {
                        this.setState({
                            suma: this.state.suma + 1,
                            // title: `${this.state.title} ${this.state.suma}`,
                        });
                        // console.log(this.state.suma);
                    }}
                />
            </View>
        );
    }
}
