import React from 'react';
import { Button, Text, View } from 'react-native';

// Props es una preferencia a las variables, const, obj, componentes, etc.
// que compoarte el componente padre conmigo
const Inicioo = (props) => {
    return (
        <View
            style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            {/* Las propiedades son elementos que tiene algún componente */}
            {/* en este caso Button tiene title y onPress  */}
            <Button
                title='Inicio'
                onPress={() => {
                    // Para navegar entre ventanas usamos:
                    // - propiedad: "navigation"
                    // - función: "navigate"
                    // - parámetro: "name del componente(screen a ir)"
                    props.navigation.navigate('Login');
                }}
            />
            <Button
                title='Registro'
                onPress={() => {
                    props.navigation.navigate('Registro');
                }}
            />
            <Button
                title='Registroo'
                onPress={() => {
                    props.navigation.navigate('Registroo');
                }}
            />
        </View>
    );
};

export default Inicioo;
