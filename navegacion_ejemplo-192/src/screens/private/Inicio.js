import { useFocusEffect } from '@react-navigation/native';
import React, { useEffect, useState } from 'react';
import { SafeAreaView, Text, View } from 'react-native';
import Snackbar from 'react-native-snackbar-component';
import firebase from './../../database/firebase';

const Inicio = (props) => {
    const [snack, setSnack] = useState(false);
    // Efecto que actualice el titulo del Stack Navigator
    // desde los componentes Drawer Hijos
    // Esto se tiene que actualizar cada vez que cambiamos de Drawer.Screen
    useFocusEffect(() => {
        // Modificamos las opciones del Header del Stack (padre)
        props.navigation.dangerouslyGetParent().setOptions({
            title: 'Inicio',
        });
    });

    // Creamos un efecto (solo se ejecute al inicio) que nos permita ir por
    // los datos del usuario que ha iniciado sesión
    useEffect(() => {
        // Verficamos si tenemos info de algún usuario de manera local
        const usuarioFBAuth = firebase.auth.currentUser;
        console.log(usuarioFBAuth);

        // Revisamos si el usuario no está validado
        if (!usuarioFBAuth.emailVerified) {
            setSnack(true);
        }
    }, []);

    return (
        // El elemento SafeAreaView revisa si un dispositivo usa notch o no
        <SafeAreaView style={{ flex: 1 }}>
            <Snackbar
                visible={snack}
                backgroundColor='#dc3545'
                textMessage='Cuenta sin verificar'
                actionText='Ok'
                actionHandler={() => {
                    setSnack(false);
                }}
            ></Snackbar>
            <Text>Inicio private</Text>
        </SafeAreaView>
    );
};

export default Inicio;
