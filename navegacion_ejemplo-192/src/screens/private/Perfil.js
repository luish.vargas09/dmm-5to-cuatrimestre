import React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FontAwesome5, MaterialIcons, Entypo } from '@expo/vector-icons';

import MisDatos from './profile/MisDatos';
import MisRentas from './profile/MisRentas';
import Terminos from './profile/Terminos';

// Para crear un TabNavigator necesitamos un contenedor para indicar dentro cada item del menú
const Tab = createBottomTabNavigator();

const Perfil = (props) => {
    return (
        <Tab.Navigator
            initialRouteName='MisDatos'
            tabBarOptions={{
                activeBackgroundColor: '#282828',
                style: { backgroundColor: '#000' },
                showLabel: false,
            }}
        >
            <Tab.Screen
                name='MisDatos'
                component={MisDatos}
                options={{
                    tabBarIcon: () => (
                        <FontAwesome5 name='user-edit' size={30} color='#fff' />
                    ),
                }}
            />
            <Tab.Screen
                name='MisRentas'
                component={MisRentas}
                options={{
                    tabBarIcon: () => (
                        <MaterialIcons
                            name='movie-filter'
                            size={30}
                            color='#fff'
                        />
                    ),
                }}
            />
            <Tab.Screen
                name='Terminos'
                component={Terminos}
                options={{
                    tabBarIcon: () => (
                        <Entypo name='text' size={30} color='#fff' />
                    ),
                }}
            />
        </Tab.Navigator>
    );
};

export default Perfil;
