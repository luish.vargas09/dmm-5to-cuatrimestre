import { useFocusEffect } from '@react-navigation/core';
import React from 'react';
import { Image, Text, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Usuario from '../../components/Usuario';

const Catalogo = (props) => {
    useFocusEffect(() => {
        props.navigation.dangerouslyGetParent().setOptions({
            title: 'Catálogo',
        });
    });

    const usuarios = [
        {
            id: 1,
            email: '2019371079@uteq.edu.mx',
            first_name: 'Luis Humberto',
            last_name: 'Vargas',
            avatar: 'https://pbs.twimg.com/media/Dyz2VlrWoAAWIsj.jpg',
        },
        {
            id: 2,
            email: '2019371089@uteq.edu.mx',
            first_name: 'Enrique',
            last_name: 'Zamudio Álvarez',
            avatar:
                'https://admin.srperro.com/media/uploads/ca83439ceb3411e2845f22000a9f3c3e_7.jpg',
        },
        {
            id: 3,
            email: 'Villafranca@uteq.edu.mx',
            first_name: 'José Luis',
            last_name: 'Villafranca',
            avatar:
                'https://admin.srperro.com/media/uploads/ca83439ceb3411e2845f22000a9f3c3e_7.jpg',
        },
        {
            id: 4,
            email: 'noe@uteq.edu.mx',
            first_name: 'Noe',
            last_name: 'Ayala Cerda',
            avatar: 'https://pbs.twimg.com/media/Dyz2VlrWoAAWIsj.jpg',
        },
    ];

    return (
        <View stye={{ flex: 1 }}>
            <FlatList
                style={{ margin: 10, marginHorizontal: 10 }}
                // El origen de la información a mostrar en la lista
                data={usuarios}
                // El diseño de la presentación de los datos de la lista por cada elemento
                // renderItem={(item) => {
                //     return <Text>{item.item}</Text>;
                // }}

                renderItem={(item) => <Usuario datosUsuario={item.item} />}
                // Indicamos el elemento que identifica a cada elemento de la colección
                keyExtractor={(item) => item.id.toString()}
            />
        </View>
    );
};

export default Catalogo;
