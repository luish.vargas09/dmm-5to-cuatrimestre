import { useFocusEffect } from '@react-navigation/core';
import React from 'react';
import { Text, View } from 'react-native';

const Rentas = (props) => {
    useFocusEffect(() => {
        props.navigation.dangerouslyGetParent().setOptions({
            title: 'Rentas',
        });
    });

    return (
        <View>
            <Text>Rentas private</Text>
        </View>
    );
};

export default Rentas;
