import React, { useEffect, useLayoutEffect, useState } from 'react';
import { Alert, BackHandler, TouchableOpacity, Dimensions } from 'react-native';

import { createDrawerNavigator } from '@react-navigation/drawer';
import { DrawerActions } from '@react-navigation/core';
import Sidebar from '../../components/Sidebar';

// Import para saber si es tablet
// import * as Device from 'expo-device';

import Inicio from './Inicio';
import Perfil from './Perfil';
import Rentas from './Rentas';
import Catalogo from './Catalogo';
import Practica3 from './Practica3';
// import CerrarSesion from './CerrarSesion';

import { Entypo, AntDesign } from '@expo/vector-icons';
import Notificaciones from './Notificaciones';
import Geoloc from './Geoloc';

// Para crear un drawer necesitamos una constante donde
// guardaremos las referencias a cada item del contenido de Screens
const Drawer = createDrawerNavigator();

const Home = (props) => {
    const [drawerType, setDrawerType] = useState('front');

    // Alerta que confirma la acción de "salir"
    const backAction = () => {
        Alert.alert(
            '¡Espera!',
            '¿Realmente deseas salir?',
            [
                {
                    text: 'Cancelar',
                    onPress: () => null,
                    style: 'cancel',
                },
                {
                    text: 'Sí, salir',
                    onPress: () => {
                        // Eliminamos el historial del Stack
                        props.navigation.reset({
                            index: 0,
                            routes: [{ name: 'Login' }],
                        });
                        props.navigation.navigate('Login');
                    },
                },
            ],
            { cancelable: false }
        );
        return true;
    }; //FIN ALERTA

    // Se pueden manipular diferentes momentos en el ciclo de vida de un Screen:
    // 1.- Antes de visualizarse
    // 2.- Al modificar el VDOM
    // 3.- Antes de destruirse
    // 4.- En su ejecución

    // Se conocen como EFECTOS (Realizan algún cambio en la UI en algún momento)

    // useEffect = Es el más común para modificar el contenido de la UI con componentes nuevos
    // useLayoutEffect = Lo usamos cuando modificamos elementos del VDOM actual

    // Cambiamos el icono de la izq del encabezado del Navigator
    // Este efecto solo se ejecutará la primera vez que se carga el componente Home
    useLayoutEffect(() => {
        // Si el ancho total de la pantalla del dispositivo
        // es igual o superior a 720px, entonces cambiamos
        // el drawer a permanent
        if (Dimensions.get('window').width >= 720) {
            setDrawerType('permanent');
        }

        props.navigation.setOptions({
            headerLeft: () =>
                drawerType === 'front' ? (
                    <TouchableOpacity
                        style={{
                            marginStart: 10,
                            paddingLeft: 10,
                            paddingVertical: 10,
                            paddingRight: 30,
                        }}
                        onPress={() => {
                            props.navigation.dispatch(
                                DrawerActions.toggleDrawer()
                            );
                        }}
                    >
                        <Entypo name='menu' size={30} />
                    </TouchableOpacity>
                ) : null,
            headerRight: () => (
                <TouchableOpacity
                    style={{
                        paddingVertical: 10,
                        paddingLeft: 30,
                        paddingRight: 20,
                        marginEnd: 10,
                    }}
                    onPress={backAction}
                >
                    <AntDesign name='poweroff' size={21} />
                </TouchableOpacity>
            ),
        });
    }, []);

    // Efecto para sobreescribir el funcionamiento del botón back
    // este código sólo se ejecutará la primera vez que cargue el componente
    useEffect(() => {
        // Vincular el evento back del SO a mi alerta Back
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction
        );
        // Al salir de Home eliminamos el evento del backButton del SO
        return () => backHandler.remove();
    }, []);
    return (
        // Creamos un contenedor de los items del Drawer
        <Drawer.Navigator
            initialRouteName='Inicio'
            drawerType={drawerType}
            openByDefault={false}
            drawerContent={() => <Sidebar {...props} />}
        >
            {/* Por cada item que necesite en el Drawer agrego un Screen */}
            <Drawer.Screen name='InicioUser' component={Inicio} />
            <Drawer.Screen name='Perfil' component={Perfil} />
            <Drawer.Screen name='Catalogo' component={Catalogo} />
            <Drawer.Screen name='Rentas' component={Rentas} />
            <Drawer.Screen name='Practica3' component={Practica3} />
            <Drawer.Screen name='Geoloc' component={Geoloc} />
            <Drawer.Screen name='Notificaciones' component={Notificaciones} />
            {/* <Drawer.Screen name='CerrarSesion' component={CerrarSesion} /> */}
        </Drawer.Navigator>
    );
};

export default Home;
