import { useFocusEffect } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { Image, RefreshControl, Text, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import Usuario from '../../components/Usuario';

const Catalogoo = (props) => {
    // Estado que guarde el arreglo de objetos de la lista de usuarios
    const [usuarios, setUsuarios] = useState([]);
    const [rcVisible, setRcVisible] = useState(true);

    useFocusEffect(() => {
        // Modificamos las opciones del Header del Stack(padre)
        props.navigation.dangerouslyGetParent().setOptions({
            title: 'Catálogo',
        });
    });

    // JS permite generar peticiones asincronas (Threads/Hilos)
    // permitiendo optimizar el tiempo de carga y espera de dichas funciones

    // Promise (Promesa): Son peticiones que esperan regresar algo, pero no es seguro que así sea
    // Las promesas tienen una particularidad de poder esperar una respuesta sin hacer que todo el programa espere con ella

    // Los CallBack son funciones que se ejecutan al finalizar cierta acción

    // Para utilizar una Promesa/Promise existen dos formas:
    // 1.- fetch con callback
    // 2.- funciones asincronas
    // 3.- Instacia de tipo Promise

    // Forma 1:
    // Tomar el contenido del servicio de lista de usuarios por medio de un callback
    const getUsuariosCallBack = () => {
        // fecth es el equivalente a ajax.url
        // 1.- Invocar por medio de fecth la url
        fetch('https://reqres.in/api/users?per_page=12')
            // 2.- Llamamos al callback, después de ir a la URL
            .then((response) => response.json())
            // 3.- Esperamos a que se obtenga la respuesta json
            .then((json) => {
                // console.log(json.data);
                // Arreglo para guardar todos los datos de los usuarios
                const arrUsuarios = [];

                // Recorremos el arreglo data para extraer todos los usuarios
                json.data.map((usuario) => {
                    // Guardamos cada usuario en el arreglo
                    // Podemos agregarle más datos si lo requerimos
                    // con el operador spread (...) toma los datos actuales
                    // y en este caso le agregamos edad
                    arrUsuarios.push({
                        ...usuario,
                        edad: 24,
                    });
                });
                // Pasamos el arreglo de usuarios al state
                setUsuarios(arrUsuarios);
            })
            // Si todo sale mal, entonces:
            .catch((e) => console.error(e));
    };

    // Usamos una función asincrona para tomar la lista de usuarios
    const getUsuariosAsync = async () => {
        // La palabra reservada await indica que el contenido de una varible/constante está en
        // espera de ser recibida. Cumple la misma función que await (pero no es un callbakc)
        try {
            const response = await fetch(
                'https://reqres.in/api/users?per_page=12'
            );
            const json = await response.json();
            // console.log(json.data);
            // // Arreglo para guardar todos los datos de los usuarios
            // const arrUsuarios = [];

            // // Recorremos el arreglo data para extraer todos los usuarios
            // json.data.map((usuario) => {
            //     // Guardamos cada usuario en el arreglo
            //     arrUsuarios.push(usuario);
            // });
            // // Pasamos el arreglo de usuarios al state
            // setUsuarios(arrUsuarios);
            setUsuarios(json.data);
            setRcVisible(false);
        } catch (e) {
            console.error(e);
        }
    };

    // Creamos un efecto que no esté enganchado a ningún componente
    // (Solo se ejecuta al inicio de la Screen)
    useEffect(() => {
        // Invocamos al servicio de lista de usuarios
        // Le quitamos el tiempo es solo para verlo nosotros
        setTimeout(() => {
            getUsuariosAsync();
        }, 1000);
    }, []);

    return (
        <View stye={{ flex: 1 }}>
            <FlatList
                style={{ margin: 15 }}
                // Indicar el estado de carga
                refreshControl={<RefreshControl refreshing={rcVisible} />}
                data={usuarios}
                renderItem={(item) => <Usuario datosUsuario={item.item} />}
                keyExtractor={(item) => item.id.toString()}
            />
        </View>
    );
};

export default Catalogoo;
