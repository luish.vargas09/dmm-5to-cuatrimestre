import React, { useState } from 'react';
import {
    Alert,
    SafeAreaView,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import ProgressDialog from './../../components/ProgressDialog';
import { MaterialIcons } from '@expo/vector-icons';

// Para mostrar un mapa con el proveedor defecto usamos
// un componente de tipo MapView desde react-native-maps
import MapView, { Callout, Marker } from 'react-native-maps';

// Elementos de localización:
import * as Location from 'expo-location';
import { LocationSubscriber } from 'expo-location/build/LocationSubscribers';
import { color } from 'react-native-reanimated';

const marcadores = [
    // {
    //     nombre: 'Jardín Guerrero',
    //     direccion:
    //         'Calle Vicente Guerrero, Colonia Centro,\nSantiago de Querétaro, MX 76000',
    //     ubicacion: { latitude: 20.5931, longitude: -100.392 },
    // },
    // {
    //     nombre: 'UTEQ',
    //     direccion:
    //         'Av. Pie de la cuesta 2051, Col Unidad Nacional\nCP.76148, Querétaro, Qro',
    //     ubicacion: { latitude: 20.6540463, longitude: -100.4061927 },
    // },
];

const Geoloc = (props) => {
    const [progress, setProgress] = useState(true);
    const [mapa, setMapa] = useState(null);

    const [home, setHome] = useState({
        ubicacion: {
            latitud: 20.5857931,
            longitud: -100.3885816,
        },
        nombre: (
            <Text>
                <MaterialIcons name='home' size={18} color='#000' /> Centro
                Educativo Cultural Gómez Morín
            </Text>
        ),
        direccion:
            'Av. Constituyentes S/N, Villas del Sur,\n76000 Santiago de Querétaro, Qro.',
    });

    // Función flecha que pida permiso de ir por la ubicación
    // (latitud y longitud del usuario)
    const getUbicacion = async () => {
        setProgress(true);
        try {
            // Pedimos el permiso de ubicación
            const { status } = await Location.requestPermissionsAsync();
            // Si los permisos son aceptados
            if (status == 'granted') {
                // Tomamos la ubicación del usuario
                const location = await Location.getCurrentPositionAsync({
                    accuracy: Location.Accuracy.Highest,
                });

                // Usamos una promesa para buscar las direcciones más cercanas
                // a las coordenadas geográficas enviadas

                // Invocamos al método reverseGeocodeAsync
                // coordenadas => dirección
                // dirección => coordenadas

                Location.reverseGeocodeAsync({
                    latitude: location.coords.latitude,
                    longitude: location.coords.longitude,
                })
                    .then((address) => {
                        // Tomar la primera dirección
                        const direccion = address[0];
                        console.log(direccion);

                        setHome({
                            ubicacion: {
                                latitud: location.coords.latitude,
                                longitud: location.coords.longitude,
                            },
                            nombre: (
                                <Text>
                                    <MaterialIcons
                                        name='home'
                                        size={18}
                                        color='#000'
                                    />{' '}
                                    Centro Educativo Cultural Gómez Morín
                                </Text>
                            ),
                            direccion: `${direccion.street}, ${direccion.district},\n${direccion.city},\nC.P. ${direccion.postalCode} ${direccion.region}, ${direccion.country}`,
                        });

                        setProgress(false);

                        // Movemos el mapa a nuestra nueva ubicación
                        mapa.animateToRegion({
                            latitude: location.coords.latitude,
                            longitude: location.coords.longitude,
                            latitudeDelta: 0.008,
                            longitudeDelta: 0.008,
                        });
                    })
                    .catch((e) => console.log(e.toString()));
            } else {
                setProgress(false);
                Alert.alert('ERROR', 'Permiso de ubicación necesario');
            }
        } catch (error) {
            setProgress(false);
            console.log(error.toString());
        }
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            {/* En este caso usamos un condicional ternario tradicional */}
            {progress === true ? <ProgressDialog /> : null}
            {/* // Condicional ternario bajo el tipo de dato */}
            {/* {progress ? <ProgressDialog /> : null} */}
            {/* Condicional verdadero (solo nos interesa el caso true)
                solo aplica si se ejecuta la condición de manera verdadera
             */}
            {/* {progress && <ProgressDialog />} */}

            {/* Coordenadas del punto de inicio 
                20.5931, -100.392  

                latitudDelta y longitudDelta 
                Representan el ángulo de curvatura de la tierra con respecto
                al plano cartesiano.

                Los valores delta van desde 1 hasta 0
                1 = más alejado
                0 = más cercano
            */}

            <View
                style={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <Text style={{ fontSize: 18 }}>{home.direccion}</Text>
            </View>

            <MapView
                // Creamos una referencia del mapa para poder utilizarlo
                // fuera del MapView
                ref={(map) => setMapa(map)}
                // showsMyLocationButton
                showsUserLocation
                followsUserLocation
                // onUserLocationChange
                style={{
                    flex: 8,
                }}
                initialRegion={{
                    latitude: home.ubicacion.latitud,
                    longitude: home.ubicacion.longitud,
                    latitudeDelta: 0.079,
                    longitudeDelta: 0.079,
                }}
                // onMapReady={() => setProgress(false)}
            >
                <Marker
                    coordinate={{
                        latitude: home.ubicacion.latitud,
                        longitude: home.ubicacion.longitud,
                    }}
                >
                    <Callout>
                        <View style={{ padding: 10 }}>
                            <Text
                                style={{
                                    fontSize: 18,
                                    marginBottom: 10,
                                }}
                            >
                                {home.nombre}
                            </Text>
                            <Text>{home.direccion}</Text>
                        </View>
                    </Callout>
                </Marker>
                {/* Agregamos marcadores */}
                {marcadores.map((m, index) => (
                    <Marker
                        key={`marcador-${index}`}
                        coordinate={{
                            latitude: m.ubicacion.latitude,
                            longitude: m.ubicacion.longitude,
                        }}
                    >
                        <Callout>
                            <View style={{ padding: 10 }}>
                                <Text
                                    style={{
                                        fontSize: 18,
                                        marginBottom: 10,
                                    }}
                                >
                                    {m.nombre}
                                </Text>
                                <Text>{m.direccion}</Text>
                            </View>
                        </Callout>
                    </Marker>
                ))}
            </MapView>
            <TouchableOpacity
                style={{
                    backgroundColor: '#000',
                    paddingHorizontal: 20,
                    paddingVertical: 10,
                    borderRadius: 10,
                    position: 'absolute',
                    zIndex: 100,
                    right: 10,
                    bottom: 10,
                    opacity: 0.7,
                    flex: 2,
                }}
                onPress={getUbicacion}
            >
                <MaterialIcons
                    name='location-searching'
                    color='#fff'
                    size={22}
                />
            </TouchableOpacity>
        </SafeAreaView>
    );
};

export default Geoloc;
