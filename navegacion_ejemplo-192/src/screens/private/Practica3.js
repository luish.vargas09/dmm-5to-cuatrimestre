import { useFocusEffect } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import { Image, RefreshControl, Text, View } from 'react-native';
import { FlatList } from 'react-native-gesture-handler';
import ToDos from '../../components/ToDos';

const Practica3 = (props) => {
    // Estado que guarde el arreglo de objetos de la lista de usuarios
    const [lista, setLista] = useState([]);
    const [rcVisible, setRcVisible] = useState(true);

    useFocusEffect(() => {
        // Modificamos las opciones del Header del Stack(padre)
        props.navigation.dangerouslyGetParent().setOptions({
            title: 'Practica3',
        });
    });

    // Usamos una función asincrona para tomar la lista de usuarios
    const getToDosAsync = async () => {
        // La palabra reservada await indica que el contenido de una varible/constante está en
        // espera de ser recibida. Cumple la misma función que await (pero no es un callbakc)
        try {
            const response = await fetch(
                'https://jsonplaceholder.typicode.com/todos?per_page=200'
            );
            const json = await response.json();
            // console.log(json.data);
            setLista(json);
            // setToDos(json.completed);
            setRcVisible(false);
        } catch (e) {
            console.error(e);
        }
    };

    // Creamos un efecto que no esté enganchado a ningún componente
    // (Solo se ejecuta al inicio de la Screen)
    useEffect(() => {
        // Invocamos al servicio de lista de usuarios
        // Le quitamos el tiempo es solo para verlo nosotros
        setTimeout(() => {
            getToDosAsync();
        }, 2000);
    }, []);

    return (
        <View stye={{ flex: 1 }}>
            <FlatList
                style={{ margin: 15 }}
                // Indicar el estado de carga
                refreshControl={<RefreshControl refreshing={rcVisible} />}
                data={lista}
                renderItem={(item) => <ToDos datosToDos={item.item} />}
                keyExtractor={(item) => item.id.toString()}
            />
        </View>
    );
};

export default Practica3;
