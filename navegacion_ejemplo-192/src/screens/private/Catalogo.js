import { useFocusEffect } from '@react-navigation/core';
import React, { useEffect, useState } from 'react';
import {
    SafeAreaView,
    FlatList,
    View,
    Text,
    ImageBackground,
} from 'react-native';
import estilos from '../../styles/estilos';
import firebase from './../../database/firebase';

const Catalogo = (props) => {
    const [pelis, setPelis] = useState([]);

    useFocusEffect(() => {
        props.navigation.dangerouslyGetParent().setOptions({
            title: 'Catálogo',
        });
    });

    // Efecto sin enganchar para mostrar la lista de peliculas
    // en la colección "peliculas"
    // Los efectos nunca deben tener una función async
    useEffect(() => {
        // Snapshot es una fotografía, una copia instantanea de la
        // versión actual de la BD que se modifica en tiempo real
        // de manera automática.
        firebase.db.collection('peliculas').onSnapshot((querySnapshot) => {
            // querySnapshot contiene un arreglo llamado "docs"
            // con todos los documentos de la colección

            // Recorrer el arreglo de "docs" para poder acceder
            // a cada elemento de manera individual
            const arrPelis = [];
            querySnapshot.docs.map((doc) => {
                // Los datos de cada documento se almacenan en
                // la función data()

                // Se usan los operadores de aspersión para decir
                // que vamos a tomar todas las cosas que hay en doc
                // y le vamos a agregar un elemento más (id).
                // El id es una propiedad y no está en el data()
                arrPelis.push({ ...doc.data(), id: doc.id });
            });

            setPelis(arrPelis);
            console.log(pelis);
            // console.log(doc.data());
            // console.log(doc.data().calificacion);
            // console.log(doc.id);
        });
    }, []);

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <FlatList
                style={{ margin: 15 }}
                data={pelis}
                keyExtractor={(item) => item.id}
                renderItem={(item) => (
                    <View
                        style={{
                            borderColor: '#000',
                            borderWidth: 1,
                            paddingVertical: 10,
                            paddingHorizontal: 20,
                            marginVertical: 10,
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        <Text style={estilos.titulo}>{item.item.titulo}</Text>
                        <ImageBackground
                            source={{ uri: item.item.poster }}
                            blurRadius={0}
                            resizeMode='contain'
                            style={{ width: 200, height: 200 }}
                        />
                        {/* <Text>{item.item.calificacion}</Text> */}
                        <Text
                            style={{
                                textAlign: 'justify',
                            }}
                        >
                            {item.item.desc}
                        </Text>
                    </View>
                )}
            />
        </SafeAreaView>
    );
};

export default Catalogo;
