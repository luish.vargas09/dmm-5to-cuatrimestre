import React, { useEffect, useState } from 'react';
import {
    Alert,
    Button,
    ImageBackground,
    Platform,
    SafeAreaView,
    Text,
    View,
} from 'react-native';
import { FontAwesome5 } from '@expo/vector-icons';
import {
    ScrollView,
    TextInput,
    TouchableOpacity,
} from 'react-native-gesture-handler';
import firebase from './../../../database/firebase';
import estilos from './../../../styles/estilos';
import ProgressDialog from '../../../components/ProgressDialog';
import Snack from 'react-native-snackbar-component';
import AppModal from '../../../components/AppModal';
import * as ImagePicker from 'expo-image-picker';
import * as Permissions from 'expo-permissions';

const MisDatos = (props) => {
    const [usuarioFirebase, setUsuarioFirebase] = useState({});
    const [docUsuario, setDocUsuario] = useState({});
    const [loading, setLoading] = useState(true);
    const [snackUpdate, setSnackUpdate] = useState(false);
    const [snackError, setSnackError] = useState(false);
    const [modalImg, setModalImg] = useState(false);

    // Creamos un hook que nos permita conectar al cargarse el screen
    // con los datos de dicho usuario desde la colección "usuarios"

    // No se debe convertir un efecto en una función async,
    // si es necesario usar un hook y una promesa, se debe crear una
    // función dentro del hook o bien crear una AF e invocarla en el hook
    useEffect(() => {
        // Tomamos los datos del usuario que ha iniciado sesión
        setUsuarioFirebase(firebase.auth.currentUser);
        // console.log(usuarioFirebase.uid);
        // console.log(firebase.auth.currentUser);

        // Invocamos a la consulta (query) creada abajo
        getDocUsuario(firebase.auth.currentUser.uid);
    }, []);

    // AF async que ejecuta una consulta sobre la colección "usuarios"
    const getDocUsuario = async (uid) => {
        try {
            // const uid = usuarioFirebase.uid;
            const query = await firebase.db
                .collection('usuarios')
                // Where usa tres parámetro:
                // 1.- Clave a comparar (campo en la "tabla")
                // 2.- Tipo de condición (leer documentación)
                // 3.- Valor de la condición
                .where('authId', '==', uid)
                .get();

            // Verificar si la consulta no está vacía
            if (!query.empty) {
                // query contiene un snapshot llamado docs
                // y es un array con todas los documentos
                // de la consulta

                // console.log(query.docs);

                // Existen dos formas de sacar el documento "docs"
                // Forma 1:
                // Cuando esperamos varios registros en una consulta
                // recorremos a doc por medio de un forEach o un map
                // query.docs.forEach((doc) => {
                //     console.log(doc.data());
                // });

                // Forma 2:
                // Cuando esparamos solo un registro
                const snapshot = query.docs[0];
                // console.log(snapshot.data());
                setDocUsuario({ ...snapshot.data(), id: snapshot.id });
                // console.log(docUsuario);
                setLoading(false);
            }
        } catch (error) {
            console.warn(error.toString());
        }
    };

    // Creamos una constante para tomar una imagen desde la galería
    // (Image Gallery - Android) | (Camera Roll - iOS)
    // 0.- Importar librería ImagePicker y todos sus componentes
    // 1.- Pedir permiso para acceder a la multimedia del dispositivo
    // 2.- Indicar el tipo de multimedia que necesitamos (photo/video/all)
    // 3.- Indicar si se podrá editar la imagen
    // 4.- Indicar la relación de aspecto
    const getImagenGaleria = async () => {
        // Paso 1: Preguntamos por el permiso para acceder a los elementos de la galería
        const {
            status,
        } = await ImagePicker.requestMediaLibraryPermissionsAsync();
        // Si el usuario nos da permiso de ingresar a su galería
        // mostramos todas sus fotos y esperamos que seleccione una
        if (status === 'granted') {
            const imgGaleria = await ImagePicker.launchImageLibraryAsync({
                // Paso 2: Indicamos el tipo de multimedia
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                // Paso 3: Indicamos si se podrá editar la imagen
                allowsEditing: true,
                // Paso 4: Indicamos la relación de aspecto a escala de 1/3
                aspect: [4, 3],
                quality: 1,
            });

            console.log(imgGaleria);
            console.log(docUsuario.avatar);

            // Agregar la url de la imagen al state docUsuario
            setDocUsuario({ ...docUsuario, ['avatar']: imgGaleria.uri });
            // Ocultaar modal de selección de fotos
            setModalImg(false);
            // Mostramos loader
            setLoading(true);
            // Subir la foto a fireabase storage
            // NOTA: asegurarnos de implementar storage desde el archivo firebase.js
            // Para subir archivos a firebase es necesario crear un objeto blob dentro
            // de un elemento File
            const blob = await (await fetch(imgGaleria.uri)).blob();

            // File tiene tres parámetros:
            // 1.- Contenido binario
            // 2.- nombre del archivo
            // 3.- config del archivo (tipo de archivo)
            const file = new File([blob], `${docUsuario.id}.jpg`, {
                type: 'image/jpeg',
            });

            blob.close();

            // Con el archivo creado podemos subirlo al storage
            // Acerca de firebase storage
            // La referencia a storage se posiciona en la raíz del contenedor
            // ref() hace referencia al contenedor (bucket)
            // child() hace referencia a un componente dentro de la referencia del contenedor
            // put() crea un nuevo archivo a partir de un blob/file
            try {
                const subida = await firebase.storage
                    .ref()
                    .child(file.name)
                    .put(file, { contentType: file.type });

                console.log(subida.state);
                // si la subida es exitosa
                if (subida.state === 'success') {
                    // Solicitar la url de la imagen
                    const urlAVatar = await subida.ref.getDownloadURL();
                    console.log(urlAVatar);
                    // Actualizamos los datos de la colección para agregar
                    // la imagen al documento del usuario
                    await firebase.db
                        .collection('usuarios')
                        .doc(docUsuario.id)
                        .update({ avatar: urlAVatar });
                    // Snack que indique los cambios
                    setLoading(false);
                    setSnackUpdate(true);
                }
            } catch (error) {
                setLoading(false);
                setSnackError(true);
                console.log(error.toString());
            }
        }
    };

    // Función para tomarnos una foto desde la cámara del dispositivo
    const getFotoCamara = async () => {
        // Para usar la cámara necesitamos dos permisos:
        // 1.- Acceso a la cámara
        // 2.- Accesp a la galería del dispositivo
        const permisoCamara = await Permissions.askAsync(Permissions.CAMERA);
        const permisoGaleria = await Permissions.askAsync(
            Permissions.MEDIA_LIBRARY
        );

        // Si obtenemos los permisos
        if (
            permisoCamara.status === 'granted' &&
            permisoGaleria.status === 'granted'
        ) {
            // Mismo procedimiento que tomar imagen de la galeria
            const imgCamara = await ImagePicker.launchCameraAsync({
                mediaTypes: ImagePicker.MediaTypeOptions.Images,
                allowsEditing: true,
                aspect: [4, 3],
                quality: 1,
            });
            // Subir la foto a fireabase storage
            // NOTA: asegurarnos de implementar storage desde el archivo firebase.js
            // Para subir archivos a firebase es necesario crear un objeto blob dentro
            // de un elemento File
            const blob = await (await fetch(imgCamara.uri)).blob();

            // File tiene tres parámetros:
            // 1.- Contenido binario
            // 2.- nombre del archivo
            // 3.- config del archivo (tipo de archivo)
            const file = new File([blob], `${docUsuario.id}.jpg`, {
                type: 'image/jpeg',
            });

            blob.close();

            // Con el archivo creado podemos subirlo al storage
            // Acerca de firebase storage
            // La referencia a storage se posiciona en la raíz del contenedor
            // ref() hace referencia al contenedor (bucket)
            // child() hace referencia a un componente dentro de la referencia del contenedor
            // put() crea un nuevo archivo a partir de un blob/file
            // TODO: Subir la foto tomada a firestore
            try {
                const fotoTomada = await firebase.storage
                    .ref()
                    .child(file.name)
                    .put(file, { contentType: file.type });

                console.log(fotoTomada.state);
                // si la subida es exitosa
                if (fotoTomada.state === 'success') {
                    // Solicitar la url de la imagen
                    const urlAVatar = await fotoTomada.ref.getDownloadURL();
                    console.log(urlAVatar);
                    // Actualizamos los datos de la colección para agregar
                    // la imagen al documento del usuario
                    await firebase.db
                        .collection('usuarios')
                        .doc(docUsuario.id)
                        .update({ avatar: urlAVatar });
                    // Snack que indique los cambios
                    setLoading(false);
                    setSnackUpdate(true);
                }
            } catch (error) {
                setLoading(false);
                setSnackError(true);
                console.log(error.toString());
            }

            // Mostrar la imagen seleccionada en el perfil
            setDocUsuario({ ...docUsuario, ['avatar']: imgCamara.uri });
            setModalImg(false);
            console.log(imgCamara.state);
        } else {
            Alert.alert(
                'ERROR',
                'Para continuar permite el uso de la cámara y tu galería',
                [
                    {
                        text: 'Continuar',
                        onPress: () => {
                            setModalImg(false);
                        },
                    },
                ]
            );
        }
    };

    return (
        // SafeAreaView calcula el espacion donde el texto no se visualiza y lo recorre
        <SafeAreaView style={{ flex: 1 }}>
            <Snack
                textMessage='Datos actualizados'
                messageColor='#fff'
                backgroundColor='#376e37'
                actionText='Entendido'
                accentColor='#5cb85c'
                actionHandler={() => {
                    setSnackUpdate(false);
                }}
                visible={snackUpdate}
            />

            <Snack
                textMessage='Ocurrió un error'
                messageColor='#fff'
                backgroundColor='red'
                actionText='Entendido'
                accentColor='#fff'
                actionHandler={() => {
                    setSnackError(false);
                }}
                visible={snackError}
            />

            {modalImg ? (
                <AppModal
                    show={modalImg}
                    layerBgColor='#333'
                    layerBgOpacity={(0, 5)}
                    modalBgColor='#fff'
                    modalOpacity={0.8}
                    modalContent={
                        <View>
                            <Text
                                style={{
                                    alignSelf: 'center',
                                    marginBottom: 20,
                                    fontSize: 20,
                                    fontWeight: '500',
                                }}
                            >
                                <FontAwesome5 name='camera-retro' size={20} />{' '}
                                Actualizar foto de perfil
                            </Text>
                            <Button
                                title='Tomar foto'
                                onPress={getFotoCamara}
                            />

                            {Platform.OS === 'android' ? (
                                <View style={{ marginVertical: 4 }}></View>
                            ) : null}

                            <Button
                                title='Galería'
                                onPress={getImagenGaleria}
                            />

                            {Platform.OS === 'android' ? (
                                <View style={{ marginVertical: 4 }}></View>
                            ) : null}

                            <Button
                                title='Cancelar'
                                color='red'
                                onPress={() => {
                                    setModalImg(false);
                                }}
                            />
                        </View>
                    }
                />
            ) : null}

            {/* Si loading es verdadero, mostramos la modal */}
            {loading ? <ProgressDialog /> : null}
            <ScrollView>
                <TouchableOpacity onPress={() => setModalImg(true)}>
                    <ImageBackground
                        // Evaluamos si el usuario tiene una imagen de perfil guardada
                        // en su doc de firestore de lo contrario mostramos la imagen
                        // defecto de perfil
                        source={
                            typeof docUsuario.avatar !== 'undefined'
                                ? { uri: docUsuario.avatar }
                                : require('./../../../../assets/images/perfil_sin_foto.png')
                        }
                        style={{
                            width: 200,
                            height: 200,
                            alignSelf: 'center',
                            marginVertical: 15,
                            borderRadius: 25,
                            overflow: 'hidden',
                        }}
                    >
                        <Text
                            style={{
                                backgroundColor: '#000',
                                color: '#fff',
                                width: '100%',
                                paddingBottom: 20,
                                paddingTop: 10,
                                textAlign: 'center',
                                opacity: 0.6,
                                position: 'absolute',
                                bottom: 0,
                            }}
                        >
                            <FontAwesome5 name='camera' color='#ffffff' />
                            {'  '}
                            Cambiar imagen
                        </Text>
                    </ImageBackground>
                </TouchableOpacity>
                <View style={{ margin: 10, flex: 1 }}>
                    <TextInput
                        style={estilos.input}
                        placeholder='Nombre'
                        keyboardType='default'
                        value={docUsuario.nombre}
                        onChangeText={(val) => {
                            setDocUsuario({
                                ...docUsuario,
                                ['nombre']: val,
                            });
                        }}
                    ></TextInput>
                    <TextInput
                        style={estilos.input}
                        placeholder='Apellido1'
                        keyboardType='default'
                        value={docUsuario.apellido1}
                        onChangeText={(val) => {
                            setDocUsuario({
                                ...docUsuario,
                                ['apellido1']: val,
                            });
                        }}
                    ></TextInput>
                    <TextInput
                        style={estilos.input}
                        placeholder='Apellido2'
                        keyboardType='default'
                        value={docUsuario.apellido2}
                        onChangeText={(val) => {
                            setDocUsuario({
                                ...docUsuario,
                                ['apellido2']: val,
                            });
                        }}
                    ></TextInput>
                    <TextInput
                        style={estilos.input}
                        placeholder='Correo electrónico'
                        keyboardType='email-address'
                        value={usuarioFirebase.email}
                        editable={false}
                    ></TextInput>

                    <Button
                        title='Guardar cambios'
                        onPress={async () => {
                            setLoading(true);
                            // Existen dos tipos de edición de datos en FireStore

                            // 1.- update (constructivo):
                            // Solo se editan los campos indicados y los demás
                            // se respetan

                            // 2.- set (desctructivo):
                            // Solo se editan los campos indicados y los demás
                            // se eliminan

                            try {
                                // Seleccionamos de toda la colección solo el
                                // elemento del id de ese documento
                                await firebase.db
                                    .collection('usuarios')
                                    .doc(docUsuario.id)
                                    .update({
                                        nombre: docUsuario.nombre,
                                        apellido1: docUsuario.apellido1,
                                        apellido2: docUsuario.apellido2,
                                    });
                                setLoading(false);
                                setSnackUpdate(true);
                            } catch (error) {
                                setLoading(false);
                                setSnackError(true);
                            }
                        }}
                    />
                </View>
            </ScrollView>
        </SafeAreaView>
    );
};

export default MisDatos;
