import React from 'react';
import { Alert, Button, Text, View } from 'react-native';
import estilos from './../styles/estilos';
import firebase from './../database/firebase';

const Inicio = (props) => {
    // Creamos una AF anónima que permita crear un documento
    // usuarioFS (FireStore) en la colección usuarios
    const crearUsuarioFS = async () => {
        try {
            // Usamos el método asíncrono collection.add
            const usuario = { nombre: 'Nuevo' };

            const usuarioFS = await firebase.db
                .collection('ejemplo')
                .add(usuario);

            console.log('¡Usuario insertado con éxito!');
            // console.log(usuarioFS);
            Alert.alert(
                'REGISTRO ÉXITOSO',
                `${usuario.nombre.toString()} te has registrado correctamente.\n\n ID: ${
                    usuarioFS.id
                }`,
                [
                    {
                        text: 'Salir',
                        onPress: () => {
                            // Eliminamos el historial del Stack
                            props.navigation.reset({
                                index: 0,
                                routes: [{ name: 'Inicio' }],
                            });
                            props.navigation.navigate('Inicio');
                        },
                    },
                    {
                        text: 'Iniciar Sesión',
                        onPress: () => {
                            setTimeout(() => {
                                props.navigation.navigate('Login');
                            }, 4000);
                        },
                    },
                ]
            );
        } catch (e) {
            console.warn(e);
        }
    };

    return (
        <View
            style={{
                flex: 1,
                alignItems: 'center',
                justifyContent: 'center',
            }}
        >
            <Text>Inicio</Text>

            <View style={{ marginVertical: 5 }}></View>
            <Button
                style={estilos.button}
                title='Login'
                onPress={() => {
                    props.navigation.navigate('Login');
                }}
            />

            <View style={{ marginVertical: 5 }}></View>
            <Button
                style={estilos.button}
                title='Registro'
                onPress={() => {
                    props.navigation.navigate('Registro');
                }}
            />

            <View style={{ marginVertical: 5 }}></View>
            <Button
                style={estilos.button}
                title='Insertar en firestore'
                onPress={crearUsuarioFS}
            />
        </View>
    );
};

export default Inicio;
