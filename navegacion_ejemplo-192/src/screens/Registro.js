import React, { Component } from 'react';
import {
    ActivityIndicator,
    Alert,
    Button,
    Image,
    ScrollView,
    Switch,
    Text,
    TextInput,
    View,
} from 'react-native';

import estilos from './../styles/estilos';
import firebase from './../database/firebase';
import get_error from '../helpers/errores_es_mx';

export default class Registro extends Component {
    // Para generar estados en una clase, debemos inicializar el objeto global "state"
    // dentro del constructor, al igual que las propiedades
    constructor(props) {
        super(props);

        this.state = {
            nombre: '',
            apellido1: '',
            apellido2: '',
            fechaNacimiento: '',
            telefono: '',
            email: '',
            pin: '',
            terminos: false,
            aiVisible: false,
            btnVisible: true,
            tiEditable: true,
        };
    }

    render() {
        const validaRegistro = () => {
            if (this.state.nombre.length < 3) {
                Alert.alert(
                    'Nombre incorrecto',
                    'Debe de contener al menos 3 caracteres',
                    [
                        {
                            text: 'Entendido',
                            onPress: () => {
                                this.setState({ nombre: '' });
                            },
                        },
                    ]
                );
                return false;
            }

            if (this.state.apellido1.length < 3) {
                Alert.alert(
                    'Apellido1 incorrecto',
                    'Debe de contener al menos 3 caracteres',
                    [
                        {
                            text: 'Entendido',
                            onPress: () => {
                                this.setState({ apellido1: '' });
                            },
                        },
                    ]
                );
                return false;
            }
            if (this.state.apellido2.length < 3) {
                Alert.alert(
                    'Apellido2 incorrecto',
                    'Debe de contener al menos 3 caracteres',
                    [
                        {
                            text: 'Entendido',
                            onPress: () =>
                                this.setState({
                                    apellido2: '',
                                }),
                        },
                    ]
                );

                return false;
            }

            if (this.state.fechaNacimiento.length != 10) {
                Alert.alert(
                    'Fecha de nacimiento incorrecta',
                    'Verifica el formato (ej: 01-01-2021)',
                    [
                        {
                            text: 'Entendido',
                            onPress: () => {
                                this.setState({ fechaNacimiento: '' });
                            },
                        },
                    ]
                );
                return false;
            }
            if (this.state.telefono.length != 10) {
                Alert.alert('Teléfono incorrecto', 'Número de 10 dígitos', [
                    {
                        text: 'Entendido',
                        onPress: () => {
                            this.setState({ telefono: '' });
                        },
                    },
                ]);
                return false;
            }
            if (!this.state.email.match('@')) {
                Alert.alert(
                    'Email incorrecto',
                    'El email ingresado no es válido, debe contener un "@"',
                    [
                        {
                            text: 'Entendido',
                            onPress: () => {
                                this.setState({ email: '' });
                            },
                        },
                    ]
                );
                return false;
            }
            if (this.state.pin.length < 6) {
                Alert.alert(
                    'PIN incorrecto',
                    'Debe de contener al menos 6 dígitos',
                    [
                        {
                            text: 'Entendido',
                            onPress: () => {
                                this.setState({ pin: '' });
                            },
                        },
                    ]
                );

                return false;
            }
            if (this.state.terminos == false) {
                Alert.alert(
                    'Términos y Condiciones',
                    'Por favor verifica los términos y condiciones',
                    [
                        {
                            text: 'Entendido',
                            onPress: null,
                        },
                    ]
                );

                return false;
            }

            // Si todo está correcto
            this.setState({
                aiVisible: true,
                btnVisible: false,
                tiEditable: false,
            });

            // Esperamos 3 segundos
            setTimeout(() => {
                this.setState({
                    aiVisible: false,
                    btnVisible: true,
                    tiEditable: true,
                });
            }, 3000);

            return true;
        };

        const crearUsuarioFS = async () => {
            try {
                // Creamos un usuario desde el servicio de auth de Firebase
                const usuarioFBAuth = await firebase.auth.createUserWithEmailAndPassword(
                    this.state.email,
                    this.state.pin
                );

                // Enviamos un email para validar la existencia de la cuenta
                await usuarioFBAuth.user.sendEmailVerification().then(() => {
                    Alert.alert(
                        `¡Registro éxitoso!`,
                        `Verifica tu cuenta de email para terminar el proceso de validación\n\nID: ${usuarioFBAuth.user.uid}`,
                        [
                            {
                                text: 'Iniciar Sesión',
                                onPress: () => {
                                    this.props.navigation.navigate('Login');
                                },
                            },
                        ],
                        { cancelable: false }
                    );
                });

                const usuario = {
                    authId: usuarioFBAuth.user.uid,
                    nombre: this.state.nombre,
                    apellido1: this.state.apellido1,
                    apellido2: this.state.apellido2,
                    fechaNacimiento: this.state.fechaNacimiento,
                    telefono: this.state.telefono,
                    terminos_aceptados: this.state.terminos,
                };

                const usuarioFS = await firebase.db
                    .collection('usuarios')
                    .add(usuario);
            } catch (e) {
                console.log(e.toString());
                Alert.alert('¡Error!', get_error(e.toString()), [
                    {
                        text: 'Corregir',
                        onPress: () => {
                            this.setState({
                                aiVisible: false,
                                btnVisible: true,
                            });
                        },
                    },
                ]);
            }
        };

        return (
            <ScrollView>
                <View
                    style={{
                        flex: 1,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                >
                    <Image
                        source={require('./../../assets/images/registro_logo.png')}
                        style={{ ...estilos.imgRegistro }}
                    />

                    <TextInput
                        placeholder={'Nombre(s)'}
                        style={estilos.input}
                        keyboardType='default'
                        value={this.state.nombre}
                        onChangeText={(val) => {
                            this.setState({ nombre: val });
                        }}
                        editable={this.state.tiEditable}
                    />

                    <View style={{ flexDirection: 'row', width: '95%' }}>
                        <View style={{ flex: 1 }}>
                            <TextInput
                                placeholder={'Apellido 1'}
                                style={estilos.input}
                                keyboardType='default'
                                value={this.state.apellido1}
                                onChangeText={(val) => {
                                    this.setState({ apellido1: val });
                                }}
                                editable={this.state.tiEditable}
                            />
                        </View>

                        <View style={{ flex: 1 }}>
                            <TextInput
                                placeholder={'Apellido 2'}
                                style={estilos.input}
                                keyboardType='default'
                                value={this.state.apellido2}
                                onChangeText={(val) => {
                                    this.setState({ apellido2: val });
                                }}
                                editable={this.state.tiEditable}
                            />
                        </View>
                    </View>

                    <TextInput
                        placeholder={'Fecha de nacimiento (DD-MM-AAAA)'}
                        style={estilos.input}
                        keyboardType='number-pad'
                        value={this.state.fechaNacimiento}
                        onChangeText={(val) => {
                            this.setState({ fechaNacimiento: val });
                        }}
                        editable={this.state.tiEditable}
                        maxLength={10}
                    />

                    <TextInput
                        placeholder={'Teléfono'}
                        style={estilos.input}
                        keyboardType='phone-pad'
                        value={this.state.telefono}
                        onChangeText={(val) => {
                            this.setState({ telefono: val });
                        }}
                        editable={this.state.tiEditable}
                        maxLength={10}
                    />

                    <TextInput
                        placeholder={'Email'}
                        style={estilos.input}
                        keyboardType='email-address'
                        value={this.state.email}
                        autoCapitalize='none'
                        onChangeText={(val) => {
                            this.setState({ email: val });
                        }}
                        editable={this.state.tiEditable}
                        maxLength={40}
                    />

                    <TextInput
                        placeholder={'PIN (6 dígitos)'}
                        style={estilos.input}
                        keyboardType='number-pad'
                        secureTextEntry
                        value={this.state.pin}
                        onChangeText={(val) => {
                            this.setState({ pin: val });
                        }}
                        editable={this.state.tiEditable}
                        maxLength={6}
                    />

                    <View
                        style={{
                            flexDirection: 'row',
                            width: '95%',
                            marginVertical: 30,
                        }}
                    >
                        <View style={{ flex: 1 }}>
                            <Text style={estilos.titulo}>
                                Acepto términos y condiciones{' '}
                            </Text>
                        </View>

                        <View style={{ flex: 1 }}>
                            <Switch
                                style={{
                                    ...estilos.switch,
                                    alignSelf: 'center',
                                }}
                                value={this.state.terminos}
                                onValueChange={(val) => {
                                    this.setState({ terminos: val });
                                }}
                                disabled={!this.state.tiEditable}
                            />
                        </View>
                    </View>

                    <ActivityIndicator
                        color='#11698e'
                        size='large'
                        style={{
                            display: this.state.aiVisible ? 'flex' : 'none',
                            marginVertical: 10,
                        }}
                    />

                    <View
                        style={{
                            display: this.state.btnVisible ? 'flex' : 'none',
                        }}
                    >
                        <Button
                            title='Enviar Datos'
                            onPress={() => {
                                validaRegistro();
                                if (validaRegistro() == true) {
                                    crearUsuarioFS();
                                }
                            }}
                        />
                        <View style={{ marginVertical: 5 }} />
                        <Button
                            title='¿Ya tienes una cuenta?'
                            onPress={() => {
                                this.props.navigation.navigate('Login');
                            }}
                        />
                        <View style={{ marginVertical: 10 }} />
                    </View>
                </View>
            </ScrollView>
        );
    }
}
