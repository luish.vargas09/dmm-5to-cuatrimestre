import React, { useState } from 'react';
import {
    ActivityIndicator,
    Alert,
    Button,
    Image,
    Text,
    TextInput,
    View,
} from 'react-native';
import firebase from './../database/firebase';
import estilos from '../styles/estilos';
import get_error from '../helpers/errores_es_mx';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { FontAwesome5, AntDesign } from '@expo/vector-icons';

const Login = (props) => {
    const [correo, setCorreo] = useState('luish.vargas09@gmail.com');
    const [pin, setPin] = useState('123456');

    // States para mostrar/ocultar ActivityIndicator
    const [btnVisible, setBtnVisible] = useState(true);
    const [aiVisible, setAiVisible] = useState(false);

    // State para deshabilidar textInput's
    const [tiHabilitado, setTiHabilitado] = useState(true);

    // Función para validación del formulario
    const validaLogin = async () => {
        // Validamos Correo
        if (correo.length < 10) {
            Alert.alert(
                'Haz ingresado un correo no válido',
                'Vuelve a intentarlo',
                [
                    {
                        text: 'Entendido',
                        onPress: () => {
                            setCorreo('');
                        },
                    },
                ],
                { cancelable: false }
            );

            return;
        }
        if (pin.length !== 6) {
            Alert.alert(
                'Pin incorrecto',
                'Debe incluir 6 dígitos',
                [
                    {
                        text: 'Entendido',
                        onPress: () => {
                            setPin('');
                        },
                    },
                ],
                { cancelable: false }
            );

            return;
        }

        // Si la validación es correcta
        // cambiamos el estado de nuestros componentes
        setAiVisible(true);
        setBtnVisible(false);
        setTiHabilitado(false);

        try {
            const usuarioFBAuth = await firebase.auth.signInWithEmailAndPassword(
                correo,
                pin
            );

            // console.log(usuarioFBAuth);
            // let mensaje = `Bienvenido ${usuarioFBAuth.user.email}`;
            // mensaje += usuarioFBAuth.user.emailVerified
            //     ? '\n¡Usuario verificado!'
            //     : '\n\n¡Por favor valida tu cuenta!';

            let mensaje = '\n\t\t\t\t\t\t\t\t\tACCESO ACTIVADO';
            Alert.alert('Bienvenido al Control Electrónico', mensaje, [
                {
                    text: 'ENTRAR',
                    onPress: () => {
                        // Después de 1 seg, habilitar todo y vamos a Home
                        setTimeout(() => {
                            // Volvemos a la normalidad
                            // Estoy agarrando señal, carnal
                            setAiVisible(false);
                            setBtnVisible(true);
                            setTiHabilitado(true);
                            props.navigation.navigate('ControlRemoto');
                        }, 500);
                    },
                },
            ]);
        } catch (e) {
            // console.log(e.code);
            Alert.alert('ERROR', get_error(e.code), [
                {
                    text: 'Corregir',
                    onPress: () => {
                        setAiVisible(false);
                        setBtnVisible(true);
                        setTiHabilitado(true);
                    },
                },
            ]);
        }
    };

    const ejemploAlert = () => {
        Alert.alert(
            //P1 = titulo
            'TITULO',
            //P2 = Mensaje
            'MENSAJE',
            //P3 = Arreglo de botones (Android, máximo 3 | IOS, ilimitado)
            [
                {
                    // Neutral Button
                    text: 'Neutral',
                    onPress: null,
                },
                {
                    // Negative Button
                    text: 'Negative',
                    onPress: null,
                    style: 'destructive',
                },
                {
                    // Positive Button
                    text: 'Positive',
                    onPress: null,
                },
            ],
            //P4 = Configuration
            { cancelable: false }
        );
    };

    return (
        <View style={estilos.contenedorGral}>
            {/* <Image
                style={estilos.imageFondo}
                source={require('./../../assets/images/cerradura_inteligente.png')}
            /> */}

            <View style={estilos.contenedor}>
                <Image
                    style={estilos.imgLogin}
                    source={require('./../../assets/images/cerradura_inteligente.png')}
                />
                <Text style={estilos.titulo}>CONTROL INTELIGENTE</Text>
                <TextInput
                    placeholder='Correo electrónico'
                    keyboardType='email-address'
                    maxLength={40}
                    style={estilos.input}
                    // Val me permite guardar lo que escribo en el input y controlarlo
                    onChangeText={(val) => setCorreo(val)}
                    value={correo}
                    editable={tiHabilitado}
                />
                <TextInput
                    placeholder='PIN'
                    keyboardType='number-pad'
                    secureTextEntry
                    maxLength={6}
                    style={estilos.input}
                    onChangeText={(val) => setPin(val)}
                    value={pin}
                    editable={tiHabilitado}
                />

                <ActivityIndicator
                    color='#000'
                    size='large'
                    style={{ display: aiVisible ? 'flex' : 'none' }}
                />

                <View
                    style={{
                        marginVertical: 10,
                        display: btnVisible ? 'flex' : 'none',
                    }}
                >
                    {/* <Button
                    style={estilos.button}
                    title='Iniciar Sesión'
                    // Va sin paréntesis para ejecutar a la referencia de la función solamente
                    // si ponemos paréntesis se ejecuta de inmediato sin apretar nada
                    // si lleva parámetros necesitamos una Arrow Function
                    onPress={validaLogin}
                /> */}

                    <View style={{ marginVertical: 5 }}></View>

                    <TouchableOpacity
                        style={estilos.boton1}
                        // Va sin paréntesis para ejecutar a la referencia de la función solamente
                        // si ponemos paréntesis se ejecuta de inmediato sin apretar nada
                        // si lleva parámetros necesitamos una Arrow Function
                        onPress={validaLogin}
                    >
                        <Text style={estilos.textoBoton}>
                            <AntDesign name='login' size={15} color='white' />
                            {'  '}
                            Iniciar Sesión
                        </Text>
                    </TouchableOpacity>

                    <TouchableOpacity
                        style={estilos.boton2}
                        onPress={() => {
                            props.navigation.navigate('Registro');
                        }}
                    >
                        <Text style={estilos.textoBoton}>
                            <FontAwesome5
                                name='user-plus'
                                size={18}
                                color='white'
                            />
                            {'  '}
                            Nuevo registro
                        </Text>
                    </TouchableOpacity>

                    {/* <Button
                    style={estilos.button}
                    title='¿No tienes una cuenta?'
                    onPress={() => {
                        props.navigation.navigate('Registro');
                    }}
                /> */}
                </View>
            </View>
        </View>
    );
};

export default Login;
