import React, { useEffect, useState } from 'react';
import { Alert, Image, SafeAreaView, Text, View } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import estilos from '../styles/estilos';
import {
    FontAwesome5,
    AntDesign,
    Entypo,
    MaterialCommunityIcons,
} from '@expo/vector-icons';
import firebase from './../database/firebase';
import Snack from 'react-native-snackbar-component';
import ProgressDialog from '../components/ProgressDialog';

const ControlRemoto = (props) => {
    const [touch, setTouch] = useState(false);
    const [cerradura, setCerradura] = useState('OFF');
    const [loading, setLoading] = useState(true);
    const [snackUpdate, setSnackUpdate] = useState(false);
    const [snackError, setSnackError] = useState(false);
    // const [docCerradura, setDocCerradura] = useState({});

    // const seguridad = firebase.db
    //     .collection('Sensores')
    //     .get()
    //     .then((querySnapshot) => {
    //         querySnapshot.forEach((doc) => {
    //             // console.log(doc.data().Cerradura);
    //             setCerradura(doc.data().Cerradura);
    //             console.log(docCerradura);
    //         });
    //     });

    const cambioClosed = async () => {
        setLoading(true);
        setCerradura('OFF');
        {
            /* Si loading es verdadero, mostramos la modal */
        }
        {
            loading ? <ProgressDialog /> : null;
        }
        // Existen dos tipos de edición de datos en FireStore

        // 1.- update (constructivo):
        // Solo se editan los campos indicados y los demás
        // se respetan

        // 2.- set (desctructivo):
        // Solo se editan los campos indicados y los demás
        // se eliminan

        try {
            // Seleccionamos de toda la colección solo el
            // elemento del id de ese documento
            await firebase.db
                .collection('Sensores')
                .doc('4Q1XR9Ik00XA5kItgN7D')
                .update({
                    Cerradura: cerradura,
                });
            setLoading(false);
            setSnackUpdate(true);
        } catch (error) {
            setLoading(false);
            setSnackError(true);
        }
    };

    const cambioOpen = async () => {
        setLoading(true);
        setCerradura('ON');
        {
            /* Si loading es verdadero, mostramos la modal */
        }
        {
            loading ? <ProgressDialog /> : null;
        }
        // Existen dos tipos de edición de datos en FireStore

        // 1.- update (constructivo):
        // Solo se editan los campos indicados y los demás
        // se respetan

        // 2.- set (desctructivo):
        // Solo se editan los campos indicados y los demás
        // se eliminan

        try {
            // Seleccionamos de toda la colección solo el
            // elemento del id de ese documento
            await firebase.db
                .collection('Sensores')
                .doc('4Q1XR9Ik00XA5kItgN7D')
                .update({
                    Cerradura: cerradura,
                });
            setLoading(false);
            setSnackUpdate(true);
        } catch (error) {
            setLoading(false);
            setSnackError(true);
        }
    };

    return (
        <SafeAreaView style={estilos.contenedorGral}>
            <Snack
                textMessage='Datos actualizados'
                messageColor='#fff'
                backgroundColor='#376e37'
                actionText='Entendido'
                accentColor='#5cb85c'
                actionHandler={() => {
                    setSnackUpdate(false);
                }}
                visible={snackUpdate}
            />

            <Snack
                textMessage='Ocurrió un error'
                messageColor='#fff'
                backgroundColor='red'
                actionText='Entendido'
                accentColor='#fff'
                actionHandler={() => {
                    setSnackError(false);
                }}
                visible={snackError}
            />

            <View style={estilos.contenedorGral}>
                <View style={estilos.contenedor}>
                    <Text
                        style={[
                            estilos.titulo,
                            { display: touch ? 'none' : 'flex' },
                        ]}
                    >
                        SEGURIDAD AL ALCANCE DE UN CLICK
                    </Text>
                    <TouchableOpacity
                        onPress={() => {
                            setTouch(true);
                            Alert.alert(
                                'REPORTE DE SEGURIDAD',
                                cerradura == 'ON'
                                    ? '\nPUERTA ABIERTA'
                                    : '\nPUERTA ASEGURADA',
                                [
                                    {
                                        text: 'Modificar',
                                        onPress: () => {
                                            setTouch(false);
                                            props.navigation.navigate(
                                                'ControlRemoto'
                                            );
                                        },
                                    },
                                    {
                                        text: 'Correcto',
                                        onPress: () => {
                                            setTouch(false);
                                            return;
                                        },
                                    },
                                ],
                                { cancelable: false }
                            );

                            return;
                        }}
                    >
                        <Text
                            style={{
                                fontSize: 22,
                                color: cerradura == 'ON' ? '#81b214' : 'red',
                                fontWeight: 'bold',
                                textAlign: 'center',
                                display: touch ? 'none' : 'flex',
                                backgroundColor: 'black',
                                paddingVertical: 10,
                            }}
                        >
                            {cerradura == 'ON'
                                ? 'PUERTA ABIERTA'
                                : 'PUERTA ASEGURADA'}
                        </Text>
                        <Image
                            style={[
                                estilos.imgFondo,
                                {
                                    display: touch ? 'none' : 'flex',
                                    width: 400,
                                    height: 300,
                                },
                            ]}
                            source={
                                cerradura == 'ON'
                                    ? require('../../assets/images/puerta_abiertaII.jpg')
                                    : require('../../assets/images/puerta_cerradaII.jpg')
                            }
                        />
                        <Text
                            style={{
                                fontSize: 22,
                                color: '#28b5b5',
                                fontWeight: 'bold',
                                textAlign: 'center',
                                display: touch ? 'none' : 'flex',
                                backgroundColor: 'black',
                                paddingVertical: 10,
                                marginBottom: 30,
                            }}
                            onPress={() => {
                                setTouch(true);
                            }}
                        >
                            <FontAwesome5 name='play-circle' size={18} />
                            {'  '}
                            VERIFICAR
                        </Text>
                    </TouchableOpacity>

                    <View style={{ flexDirection: 'row' }}>
                        <TouchableOpacity
                            style={[
                                estilos.botonCerrar,
                                {
                                    display: touch ? 'none' : 'flex',
                                    marginHorizontal: 5,
                                },
                            ]}
                            onPress={cambioClosed}
                        >
                            <Text style={estilos.textoBoton}>
                                <FontAwesome5
                                    name='door-closed'
                                    size={18}
                                    color='white'
                                />{' '}
                                CERRAR
                            </Text>
                        </TouchableOpacity>

                        <TouchableOpacity
                            style={[
                                estilos.botonAbrir,
                                {
                                    display: touch ? 'none' : 'flex',
                                    marginHorizontal: 5,
                                },
                            ]}
                            onPress={cambioOpen}
                        >
                            <Text style={estilos.textoBoton}>
                                <FontAwesome5
                                    name='door-open'
                                    size={18}
                                    color='white'
                                />{' '}
                                ABRIR
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity
                        style={[
                            estilos.boton1,
                            { display: touch ? 'none' : 'flex' },
                        ]}
                        onPress={() => {
                            props.navigation.navigate('Login');
                        }}
                    >
                        <Text style={estilos.textoBoton}>
                            <MaterialCommunityIcons
                                name='logout'
                                size={18}
                                color='white'
                            />{' '}
                            SALIR
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    );
};

export default ControlRemoto;
