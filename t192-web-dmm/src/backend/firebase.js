import firebase from 'firebase';

// Your web app's Firebase configuration
const firebaseConfig = {
    apiKey: 'AIzaSyDtHLa4suh_rm11gLB0Jk00n1zsIiluvQ8',
    authDomain: 'dmm-192-9b419.firebaseapp.com',
    projectId: 'dmm-192-9b419',
    storageBucket: 'dmm-192-9b419.appspot.com',
    messagingSenderId: '291845008193',
    appId: '1:291845008193:web:1cf69de9e830a78f8d0fc0',
};
// Initialize Firebase
firebase.initializeApp(firebaseConfig);

const backend = {
    db: firebase.firestore(),
    auth: firebase.auth(),
    storage: firebase.storage(),
};

export default backend;
