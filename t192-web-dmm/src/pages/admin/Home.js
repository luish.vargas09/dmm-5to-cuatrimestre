import React, { Fragment } from 'react';

const Home = (props) => {
    return (
        <Fragment>
            <div className='col-lg-12 grid-margin stretch-card'>
                <div className='card'>
                    <div className='card-body'>
                        <h4 className='card-title'>Home</h4>
                        <p className='card-description'>En la sección Home</p>
                    </div>
                </div>
            </div>
            <div className='col-lg-12 grid-margin stretch-card'>
                <div className='card'>
                    <div className='card-body'>
                        <h4 className='card-title'>Home</h4>
                        <p className='card-description'>En la sección Home</p>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default Home;
