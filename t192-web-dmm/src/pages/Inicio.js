import React from 'react';
import { NavLink } from 'react-router-dom';

const Inicio = (props) => {
    return (
        <div className='container'>
            <div
                className='d-flex justify-content-center'
                style={{
                    marginTop: '5%',
                    opacity: 0.8,
                }}
            >
                <div className='col-8 col-sm-10 col-md-6 col-lg-6 col-xl-6'>
                    <div className='card'>
                        <img
                            src='images/logo.png'
                            className='card-img-top m-auto py-5'
                            alt='Inicio'
                            style={{ maxWidth: '250px' }}
                        />
                        <div className='card-body'>
                            <h3 className='card-title text-center mb-5'>
                                Bienvenido AppPelis React
                            </h3>
                            {/* NavLink permite: 
                                1.- pasar parámentros 
                                2.- reemplaza <a> y tiene links nativos de React Router DOM */}
                            <NavLink
                                to='/Login'
                                className='btn btn-lg btn-primary d-block w-100 mb-3'
                            >
                                <i className='fa fa-user mx-2' />
                                Login
                            </NavLink>
                            <NavLink
                                to='/registro'
                                className='btn btn-lg btn-warning d-block w-100 mb-3 text-white'
                            >
                                <i className='fa fa-user-plus mx-2 text-white' />
                                Registro
                            </NavLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Inicio;
