import React from 'react';
import Inicio from './components/pages/Inicio';
import Login from './components/pages/Login';
import Registro from './components/pages/Registro';

function App_ejemplo_Componentes() {
    return (
        // En ReactJS no utilizamos class para referirnos a una clase // de
        // diseño css, en su lugar utilizamos 'clasName'
        <div className='container mt-5'>
            {/* Flex permite indicar que el contenido debe acomodarse al numero de elementos agregados */}
            <div className='row'>
                <div className='col-lg-4 col-sm-12 mb-5'>
                    <Inicio />
                </div>

                <div className='col-lg-4 col-sm-12 mb-5'>
                    <Login />
                </div>

                <div className='col-lg-4 col-sm-12 mb-5'>
                    <Registro />
                </div>
            </div>
        </div>
    );
}

export default App_ejemplo_Componentes;
